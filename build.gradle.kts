import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.21"
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "org.melanxoluk"
version = "0.1"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("com.uchuhimo:konf-core:0.20.0")
    implementation("com.uchuhimo:konf-yaml:0.20.0")

    implementation("org.melanxoluk:daccess:0.2.7")
    implementation("com.zaxxer:HikariCP:3.3.1")
    implementation("com.h2database:h2:1.4.199")

    implementation("io.ktor:ktor-server-netty:1.2.4")
    implementation("io.ktor:ktor-jackson:1.2.4")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.apiVersion = "1.3"
    kotlinOptions.languageVersion = "1.3"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<ShadowJar> {
    manifest {
        attributes["Main-Class"] = "org.melanxoluk.infra.StartKt"
    }

    archiveBaseName.set("infra")
    archiveClassifier.set("")
    archiveVersion.set("")
}
