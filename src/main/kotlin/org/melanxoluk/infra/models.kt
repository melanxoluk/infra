package org.melanxoluk.infra


data class Infra(
    val gitlabs: Map<Long, Gitlab>,
    val telegrams: Map<Long, Telegram>,
    val projects: Map<Long, Project>) {
    constructor(
        gitlabs: List<Gitlab>,
        telegrams: List<Telegram>,
        projects: List<Project>) : this(
        gitlabs.map { it.id to it }.toMap(),
        telegrams.map { it.id to it }.toMap(),
        projects.map { it.id to it }.toMap())
}

data class Gitlab(
    val id: Long,
    val name: String,
    val host: String,
    val accessToken: String,
    val username: String?,
    val password: String?)

data class Telegram(
    val id: Long,
    val name: String,
    val token: String,
    val channel: String)

data class Project(
    val id: Long,
    val name: String,
    val gitlab: Long,
    val telegram: Long?,
    val gitlabProjectId: Long?,
    val releasesTelegram: Long?)

// cloudflare
// hosts

// other services
// - app creator: how to make windows service from .jar
// - deployer
// - ignis fatuus
// -
