package org.melanxoluk.infra

import com.uchuhimo.konf.ConfigSpec


object InfraConfig : ConfigSpec("infra") {
    val dbName by required<String>("dbName")
    val auth by required<String>("auth")
    val host by required<String>("host")
    val port by required<Int>("port")
}
