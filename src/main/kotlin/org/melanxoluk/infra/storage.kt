package org.melanxoluk.infra

import org.melanxoluk.daccess.DAccess
import org.melanxoluk.daccess.repository
import javax.sql.DataSource


class Storage(ds: DataSource): DAccess(ds) {
    val gitlabs = repository<Gitlab>()
    val telegrams = repository<Telegram>()
    val projects = repository<Project>()
}
