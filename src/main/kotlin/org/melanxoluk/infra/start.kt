package org.melanxoluk.infra

import com.fasterxml.jackson.databind.SerializationFeature
import com.uchuhimo.konf.Config
import com.uchuhimo.konf.source.yaml
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.header
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.melanxoluk.daccess.*
import org.melanxoluk.daccess.repository.Vals
import org.melanxoluk.daccess.repository.Value
import kotlin.reflect.jvm.jvmErasure


fun main(args: Array<String>) {
    val config =
        Config { addSpec(InfraConfig) }
            .from.yaml.file("infra.yaml")
            .from.systemProperties()
            .from.env()

    val dbName = config[InfraConfig.dbName]

    val ds = HikariDataSource(HikariConfig().apply {
        jdbcUrl = "jdbc:h2:file:./$dbName"
    })

    val storage = Storage(ds)

    embeddedServer(Netty, applicationEngineEnvironment {
        connector {
            host = config[InfraConfig.host]
            port = config[InfraConfig.port]
        }

        module {
            install(ContentNegotiation) {
                jackson {
                    configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, false)
                }
            }

            routing {
                get("/ping") { call.respond("pong") }

                val auth = config[InfraConfig.auth]
                intercept(ApplicationCallPipeline.Call) {
                    val authParam = call.request.header("auth") ?: call.request.queryParameters["auth"] ?: ""
                    if (auth != authParam) {
                        call.respond(HttpStatusCode.Unauthorized)
                        return@intercept
                    }
                }

                crud(storage.gitlabs)
                crud(storage.telegrams)
                crud(storage.projects)

                get("/infra") {
                    call.respond(
                        Infra(
                            storage.gitlabs.select(),
                            storage.telegrams.select(),
                            storage.projects.select()))
                }
            }
        }
    }).start(false)
}

inline fun <reified T : Any> Route.crud(rep: Repository<T>, typeInfo: TypeInfo<T> = TypeInfo(T::class)) {
    val resource = typeInfo.clazz.simpleName!!.toLowerCase()

    get("/$resource") {
        call.respond(rep.select())
    }

    route("/$resource") {
        get("/create") {
            val vals = Vals(typeInfo.parameters.mapIndexed { i, param ->
                val name = param.name!!
                val raw = call.request.queryParameters[name]
                val value: Any? = when {
                    name == "id" -> raw?.toLong()
                    param.type.jvmErasure == Integer::class -> raw?.toInt()
                    param.type.jvmErasure == Long::class -> raw?.toLong()
                    else -> raw
                }
                Value(name.name, i, value)
            }.toMutableList())

            call.respond(rep.create(mapVals(typeInfo, vals)))
        }

        get("/delete") {
            val id = call.request.queryParameters["id"]
            if (id == null) {
                call.respond(HttpStatusCode.BadRequest)
                return@get
            }

            rep.select()
                .map { makeVals(typeInfo, it) }
                .filter { it["id".name].value.toString() == id }
                .forEach { rep.delete(mapVals(typeInfo, it)) }

            call.respond(HttpStatusCode.OK)
        }
    }
}
